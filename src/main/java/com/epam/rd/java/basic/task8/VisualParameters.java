package com.epam.rd.java.basic.task8;

public class VisualParameters {
    protected String stemColour;
    protected String leafColour;
    protected AveLenFlower aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        if (aveLenFlower==null)aveLenFlower=new AveLenFlower();
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public static class AveLenFlower {
        private int value;
        private String measure;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }
    }
}

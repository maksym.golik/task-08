package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.constants.Constants;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Flowers flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers(){return flowers;}

	public void parse (boolean validate) throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);

		if (validate) {
			factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, new SAXHandler());

	}

	private class SAXHandler extends DefaultHandler{
		private String curName;
		@Override
		public void startDocument() {
			flowers = new Flowers();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			curName=qName.trim().intern();
			switch (curName) {
				case "":
					return;
				case "flower":
					flowers.getFlower().add(new Flower());
					break;
				case "aveLenFlower":
					flowers.getFlower().get(flowers.getFlower().size() - 1).getVisualParameters().getAveLenFlower().setMeasure(attributes.getValue(0));
					break;
				case "tempreture":
					flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getTempreture().setMeasure(attributes.getValue(0));
					break;
				case "lighting":
					flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getLighting().setLightRequiring(attributes.getValue(0));
					break;
				case "watering":
					flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getWatering().setMeasure(attributes.getValue(0));
					break;
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			String data = new String(ch,start,length);
			if(data.isBlank()) {
			}
			else if(curName.equals("name"))flowers.getFlower().get(flowers.getFlower().size()-1).setName(data);
			else if(curName.equals("soil"))flowers.getFlower().get(flowers.getFlower().size()-1).setSoil(data);
			else if(curName.equals("origin"))flowers.getFlower().get(flowers.getFlower().size()-1).setOrigin(data);
			else if(curName.equals("stemColour"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().setStemColour(data);
			else if(curName.equals("leafColour"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().setLeafColour(data);
			else if(curName.equals("aveLenFlower"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(data));
			else if(curName.equals("tempreture"))flowers.getFlower().get(flowers.getFlower().size()-1).getGrowingTips().getTempreture().setValue(Integer.parseInt(data));
			else if(curName.equals("watering"))flowers.getFlower().get(flowers.getFlower().size()-1).getGrowingTips().getWatering().setValue(Integer.parseInt(data));
			else if (curName.equals("multiplying"))flowers.getFlower().get(flowers.getFlower().size()-1).setMultiplying(data);
		}
	}

	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
		SAXController saxController = new SAXController("input.xml");
		saxController.parse(false);
		System.out.println();
	}

}
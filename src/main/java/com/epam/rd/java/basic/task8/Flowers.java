package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private final List<Flower> flower;

    public Flowers(){
        flower = new ArrayList<>();
    }

    public List<Flower> getFlower(){
        return flower;
    }
}
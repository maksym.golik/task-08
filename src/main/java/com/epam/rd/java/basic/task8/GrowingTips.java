package com.epam.rd.java.basic.task8;

public class GrowingTips {
    private Tempreture tempreture;
    private Lighting lighting;
    private Watering watering;

    public Tempreture getTempreture() {
        if(tempreture==null)tempreture=new Tempreture();
        return tempreture;
    }

    public void setTempreture(Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting() {
        if(lighting==null)lighting=new Lighting();
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        if(watering==null)watering=new Watering();
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    public static class Tempreture {
        private int value;
        private String measure;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }
    }

    public static class Lighting {
        private String lightRequiring;

        public String getLightRequiring() {
            return lightRequiring;
        }

        public void setLightRequiring(String lightRequiring) {
            this.lightRequiring = lightRequiring;
        }
    }

    public static class Watering {
        private int value;
        private String measure;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }
    }
}

package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getVisualParameters() {
        if(visualParameters==null)visualParameters=new VisualParameters();
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        if(growingTips==null)growingTips=new GrowingTips();
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }
}

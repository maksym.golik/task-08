package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import com.epam.rd.java.basic.task8.constants.Constants;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers(){return flowers;}

	public void parse (boolean validate) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();


		dbf.setNamespaceAware(true);
		if(validate){
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new DefaultHandler(){
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});

		Document document = db.parse(xmlFileName);
		flowers = getFlowersFromDOM(document);

	}

	private Flowers getFlowersFromDOM (Document document){
		NodeList nodeList = document.getElementsByTagName("flower");
		Flowers result = new Flowers();
		for(int i = 0; i<nodeList.getLength(); i++){
			Element flowerDOM = (Element) nodeList.item(i);
			Flower flower = new Flower();
			flower.setName(flowerDOM.getElementsByTagName("name").item(0).getTextContent());
			flower.setSoil(flowerDOM.getElementsByTagName("soil").item(0).getTextContent());
			flower.setOrigin(flowerDOM.getElementsByTagName("origin").item(0).getTextContent());
			flower.getVisualParameters().setLeafColour(flowerDOM.getElementsByTagName("leafColour").item(0).getTextContent());
			flower.getVisualParameters().setStemColour(flowerDOM.getElementsByTagName("stemColour").item(0).getTextContent());
			flower.getVisualParameters().getAveLenFlower().setMeasure(flowerDOM.getElementsByTagName("aveLenFlower").item(0).getAttributes().item(0).getTextContent());
			flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(flowerDOM.getElementsByTagName("aveLenFlower").item(0).getTextContent()));
			flower.getGrowingTips().getWatering().setMeasure(flowerDOM.getElementsByTagName("watering").item(0).getAttributes().item(0).getTextContent());
			flower.getGrowingTips().getWatering().setValue(Integer.parseInt(flowerDOM.getElementsByTagName("watering").item(0).getTextContent()));
			flower.getGrowingTips().getTempreture().setMeasure(flowerDOM.getElementsByTagName("tempreture").item(0).getAttributes().item(0).getTextContent());
			flower.getGrowingTips().getTempreture().setValue(Integer.parseInt(flowerDOM.getElementsByTagName("tempreture").item(0).getTextContent()));
			flower.getGrowingTips().getLighting().setLightRequiring(flowerDOM.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent());
			flower.setMultiplying(flowerDOM.getElementsByTagName("multiplying").item(0).getTextContent());
			result.getFlower().add(flower);
		}
		return result;
	}

	public void saveToXmlFile(Flowers flowersList, String xmlFileName) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();
		Element flowersDOM = document.createElement("flowers");
		flowersDOM.setAttribute("xmlns","http://www.nure.ua");
		flowersDOM.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		flowersDOM.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");
		flowersList.getFlower().forEach(flower -> {
			Element flowerDOM = document.createElement("flower");
			addTextToDOM(flowerDOM,"name",flower.getName(),document);
			addTextToDOM(flowerDOM,"soil",flower.getSoil(),document);
			addTextToDOM(flowerDOM,"origin",flower.getOrigin(),document);
			Element visualParametersDOM = document.createElement("visualParameters");
			addTextToDOM(visualParametersDOM,"stemColour",flower.getVisualParameters().getStemColour(),document);
			addTextToDOM(visualParametersDOM,"leafColour",flower.getVisualParameters().getLeafColour(),document);
			addAttributeWithContentToDOM(visualParametersDOM,"aveLenFlower","measure",flower.getVisualParameters().getAveLenFlower().getMeasure(),String.valueOf(flower.getVisualParameters().getAveLenFlower().getValue()),document);
			flowerDOM.appendChild(visualParametersDOM);
			Element growingTipsDOM = document.createElement("growingTips");
			addAttributeWithContentToDOM(growingTipsDOM,"tempreture","measure",flower.getGrowingTips().getTempreture().getMeasure(),String.valueOf(flower.getGrowingTips().getTempreture().getValue()),document);
			addAttributeWithOutContentToDOM(growingTipsDOM,"lighting","lightRequiring",flower.getGrowingTips().getLighting().getLightRequiring(),document);
			addAttributeWithContentToDOM(growingTipsDOM,"watering","measure",flower.getGrowingTips().getWatering().getMeasure(),String.valueOf(flower
					.getGrowingTips().getWatering().getValue()),document);
			flowerDOM.appendChild(growingTipsDOM);
			addTextToDOM(flowerDOM,"multiplying",flower.getMultiplying(),document);
			flowersDOM.appendChild(flowerDOM);
		});
		document.appendChild(flowersDOM);

		StreamResult streamResult = new StreamResult(new File(xmlFileName));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		transformer.transform(new DOMSource(document),streamResult);
	}

	private void addTextToDOM(Element parentDOM, String elementName, String data, Document document){
		Element elementDOM = document.createElement(elementName);
		elementDOM.setTextContent(data);
		parentDOM.appendChild(elementDOM);

	}
	private void addAttributeWithContentToDOM(Element parentDOM,String elementName,String attributeName,String data, String value, Document document){
		Element elementDOM = document.createElement(elementName);
		elementDOM.setAttribute(attributeName,data);
		elementDOM.setTextContent(value);
		parentDOM.appendChild(elementDOM);
	}

	private void addAttributeWithOutContentToDOM(Element parentDOM,String elementName,String attributeName,String data, Document document){
		Element elementDOM = document.createElement(elementName);
		elementDOM.setAttribute(attributeName,data);
		parentDOM.appendChild(elementDOM);
	}
}

package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	private Flowers flowers;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers(){return flowers;}

	public Flowers parse() throws XMLStreamException {

		String curName = "";

		XMLInputFactory factory = XMLInputFactory.newFactory();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE,true);
		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()){
			XMLEvent event = reader.nextEvent();
			if(event.isStartDocument()) flowers = new Flowers();
			else if(event.isStartElement()){
				curName = event.asStartElement().getName().getLocalPart().trim().intern();
				switch (curName) {
					case "":
						;
						break;
					case "flower":
						flowers.getFlower().add(new Flower());
						break;
					case "aveLenFlower":
						flowers.getFlower().get(flowers.getFlower().size() - 1).getVisualParameters().getAveLenFlower().setMeasure(event.asStartElement().getAttributes().next().getValue());
						break;
					case "tempreture":
						flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getTempreture().setMeasure(event.asStartElement().getAttributes().next().getValue());
						break;
					case "lighting":
						flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getLighting().setLightRequiring(event.asStartElement().getAttributes().next().getValue());
						break;
					case "watering":
						flowers.getFlower().get(flowers.getFlower().size() - 1).getGrowingTips().getWatering().setMeasure(event.asStartElement().getAttributes().next().getValue());
						break;
				}
			}
			else if (event.isCharacters()) {
				String data = event.asCharacters().getData().trim();
				if(data.isBlank());
				else if(curName.equals("name"))flowers.getFlower().get(flowers.getFlower().size()-1).setName(data);
				else if(curName.equals("soil"))flowers.getFlower().get(flowers.getFlower().size()-1).setSoil(data);
				else if(curName.equals("origin"))flowers.getFlower().get(flowers.getFlower().size()-1).setOrigin(data);
				else if(curName.equals("stemColour"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().setStemColour(data);
				else if(curName.equals("leafColour"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().setLeafColour(data);
				else if(curName.equals("aveLenFlower"))flowers.getFlower().get(flowers.getFlower().size()-1).getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(data));
				else if(curName.equals("tempreture"))flowers.getFlower().get(flowers.getFlower().size()-1).getGrowingTips().getTempreture().setValue(Integer.parseInt(data));
				else if(curName.equals("watering"))flowers.getFlower().get(flowers.getFlower().size()-1).getGrowingTips().getWatering().setValue(Integer.parseInt(data));
				else if (curName.equals("multiplying"))flowers.getFlower().get(flowers.getFlower().size()-1).setMultiplying(data);
			}
		}
		reader.close();
		return flowers;
	}

	public static void main(String[] args) throws XMLStreamException {
		STAXController staxController = new STAXController("input.xml");
		staxController.parse();
		System.out.println();
	}
}